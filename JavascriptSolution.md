# JavscriptSolution

 * Javascript for..in on Array Prototype from Procom-issue#30

     "for...in" is loop only iterates a specified variable over all properties of an object, in arbitrary order

     so if you added a function to "Array" prototype the problem is you will found that function every time when you use for...in on Array object when u include file that added function to "Array" prototype
     
     solution solved is use "ArrayObject.forEach" instead of "for...in"