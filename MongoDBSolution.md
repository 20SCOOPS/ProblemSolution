# MongoDBSolution

## Mongoose

* Default Date.now() Doesn't go well from Procom-issue#13

    it because we didn't set Timezone for mongodb to storing as local Timezone mongodb is storing in UTC format

    solution solved is use "Date.now()" on local timezone in javascript

* Data recieved after Update on Mongoose(MongoDB) from Procom-issue#32 
    
    The default mongoose is to return no modify data from Model schema
    
    solution solved is have to pass and additional agurment {new : true} (default is false)

* Aggregation use in order sort>skip>limit or else data list will not right

    - sample: [3,7,6,1,4,8,2,9,5] with sort:1,skip:3,limit,2
        - right aggregation order
            - sort:1 -> [1,2,3,4,5,6,7,8,9]
            - skip:3 -> [4,5,6,7,8,9]
            - limit:2 -> [4,5] *RIGHT*
    
        - wrong aggregation order
            - limit:2 -> [3,7]
            - sort:1 -> [3,7]
            - skip:3 -> [] *WRONG*